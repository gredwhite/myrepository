package com.peterservice.palindrome;


import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by redwhite on 03.10.2015.
 */
public class UsersHolder {
    private Set<User> users = new HashSet<>();

    /**
     * Adds new user
     *
     * @param user for adding
     */
    public void addUser(User user) {
        users.add(user);
    }

    /**
     * Methods try to find user by name
     *
     * @param userName for search
     * @return search result
     */
    public Optional<User> getUserByName(String userName) {
        return users.stream()
                .filter(i -> userName.equals(i.getName()))
                .findFirst();
    }

    /**
     * Returns top game result (sorted)
     *
     * @param count - limit of results
     * @return top result - userName and result in single entry
     */
    public Map<String, Integer> getScores(int count) {
        return users.stream()
                .sorted((u1, u2) -> u2.getScore().compareTo(u1.getScore()))
                .limit(count)
                .collect(Collectors.toMap((User::getName), (User::getScore), (k, v) -> {
                            throw new RuntimeException(String.format("Duplicate key %s", k));
                        },
                        LinkedHashMap::new));
    }
}
