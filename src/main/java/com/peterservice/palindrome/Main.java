package com.peterservice.palindrome;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by redwhite on 03.10.2015.
 */
public class Main {
    /**
     * Simplest menu
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Please type your name:");
        try (Scanner scanner = new Scanner(System.in)) {
            scanner.useDelimiter("\\r?\\n");
            String userName = scanner.next();
            Game game = new Game(userName);
            AtomicInteger atomicInteger = new AtomicInteger();
            int select = 0;
            do {
                System.out.println("Please select menu item");
                System.out.println("1 - suggest word, 2 - change user, 3 - my score, 4 - my word list, 5 - records, 6 - exit");
                try {
                    select = scanner.nextInt();
                } catch (InputMismatchException e) {
                    scanner.next(); //we should read erroneous
                    System.out.println("Error. Please input number.");
                    continue;
                }

                switch (select) {
                    case 1:
                        System.out.print("Word:");
                        String word = scanner.next();
                        if (game.suggestWord(word)) {
                            System.out.println("Accepted: your score - " + game.getCurrentUserScore());
                        } else {
                            System.out.println("Rejected: Word already exists in your list or it is not palindrome");
                            System.out.println("Your score - " + game.getCurrentUserScore());
                        }
                        break;
                    case 2:
                        System.out.print("Name:");
                        String name = scanner.next();
                        game.changeUser(name);
                        System.out.println("User changed successfully. Your score - " + game.getCurrentUserScore());
                        break;
                    case 3:
                        System.out.println("Your score - " + game.getCurrentUserScore());
                        break;
                    case 4:
                        System.out.println("Accepted words:");
                        game.getCurrentUserAcceptedWords().forEach(System.out::println);
                        break;
                    case 5:
                        atomicInteger.set(1);
                        game.getScores().forEach((k, v) -> System.out.println("#" + atomicInteger.getAndIncrement() + ". name: " + k + " , score: " + v));
                        break;
                    case 6:
                        System.out.println("Goodbye! thanks for the game");
                        break;
                    default:
                        System.out.println("You selected nonexistent menu item. Please try one more time.");
                }

            } while (select != 6);
        }
    }
}

