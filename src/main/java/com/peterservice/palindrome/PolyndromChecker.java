package com.peterservice.palindrome;

/**
 * Created by redwhite on 03.10.2015.
 */
public class PolyndromChecker {
    /**
     * @param polyndromCandidate string fir testing whether palindrome  it or not
     * @return true if argument is palindrome and false otherwise
     */
    public static boolean check(String polyndromCandidate) {
        return polyndromCandidate.toLowerCase().equals(
                new StringBuffer(polyndromCandidate).reverse().toString().toLowerCase());
    }
}
