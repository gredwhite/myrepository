package com.peterservice.palindrome;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsersHolderTest {

    @Mock
    User mockUser1;
    @Mock
    User mockUser2;
    @Mock
    User mockUser3;
    @Mock
    User mockUser4;
    @InjectMocks
    private UsersHolder usersHolderWithMockedUsers = new UsersHolder();
    private UsersHolder usersHolder = new UsersHolder();
    @Mock
    private Set<User> users;

    @Test
    public void testAddUser() {
        MockitoAnnotations.initMocks(this);
        User user = new User("user1");

        usersHolderWithMockedUsers.addUser(user);

        verify(users).add(user);
    }

    @Test
    public void testGetUserByName() {
        User user1 = new User("user1");
        User user2 = new User("user2");
        usersHolder.addUser(user1);
        usersHolder.addUser(user2);

        assertEquals(Optional.of(user2), usersHolder.getUserByName("user2"));
    }

    @Test
    public void testGetScores() {
        usersHolder.addUser(mockUser1);
        usersHolder.addUser(mockUser2);
        usersHolder.addUser(mockUser3);
        usersHolder.addUser(mockUser4);

        when(mockUser1.getScore()).thenReturn(10);
        when(mockUser2.getScore()).thenReturn(5);
        when(mockUser3.getScore()).thenReturn(8);
        when(mockUser4.getScore()).thenReturn(1);
        when(mockUser1.getName()).thenReturn("user1");
        when(mockUser2.getName()).thenReturn("user2");
        when(mockUser3.getName()).thenReturn("user3");
        when(mockUser4.getName()).thenReturn("user4");

        Map<String, Integer> scores = usersHolder.getScores(2);

        assertEquals(2, scores.values().size());
        assertTrue(scores.values().contains(10));
        assertTrue(scores.values().contains(8));
    }
}