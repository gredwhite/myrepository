package com.peterservice.palindrome;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by redwhite on 03.10.2015.
 */
public class Game {
    static final int TOP_USER_COUNT = 5;
    private UsersHolder usersHolder = new UsersHolder();
    private User currentUser;

    public Game(String userName) {
        createNewUser(userName);
    }

    /**
     * @param word to add to user
     * @return true if word added and false otherwise
     */
    public boolean suggestWord(String word) {
        return PolyndromChecker.check(word) && currentUser.addWord(word.toLowerCase());
    }

    /**
     * Method for change current user
     *
     * @param name new user name
     */
    public void changeUser(String name) {
        Optional<User> userOptional = usersHolder.getUserByName(name);
        if (userOptional.isPresent()) {
            currentUser = userOptional.get();
        } else {
            createNewUser(name);
        }
    }

    /**
     * Method for creating new user
     *
     * @param name new user name
     */
    //@VisibleForTesting
    void createNewUser(String name) {
        currentUser = new User(name);
        usersHolder.addUser(currentUser);
    }

    /**
     * @return current user score
     */
    public int getCurrentUserScore() {
        return currentUser.getScore();
    }

    /**
     * @return current user accepted words
     */
    public Set<String> getCurrentUserAcceptedWords() {
        return currentUser.getWords();
    }

    /**
     * @return top results among all users
     */
    public Map<String, Integer> getScores() {
        return usersHolder.getScores(TOP_USER_COUNT);
    }
}
