package com.peterservice.palindrome;


import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class UserTest {

    public static final String USER_NAME = "Vasya";
    public static final String ANOTHER_USER_NAME = "PETYA";

    private User user;

    @Before
    public void initialize() {
        user = new User(USER_NAME);
    }

    @Test
    public void testAddWords() throws Exception {
        user.addWord("1");
        user.addWord("121");

        Set words = new HashSet<>();
        words.add("1");
        words.add("121");

        assertThat(user.getWords(), is(words));
    }

    @Test
    public void testGetScore() {
        user.addWord("1");
        user.addWord("121");
        user.addWord("qwertytrewq");

        assertEquals(user.getScore().intValue(), 15);
    }

    @Test
    public void shouldReturnTrueIfNamesSameEqualsTest() {
        User another = new User(USER_NAME);
        assertTrue(user.equals(another));
        assertEquals(user.hashCode(), another.hashCode());
    }

    @Test
    public void shouldReturnFalseIfNamesDifferentEqualsTest() {
        User another = new User(ANOTHER_USER_NAME);
        assertFalse(user.equals(another));
    }

    @Test
    public void shouldNotAffectNotNameFieldEqualsTest() {
        User another = new User(USER_NAME);
        another.addWord("12321");
        another.addWord("asdfdsa");

        user.addWord("trert");
        assertTrue(user.equals(another));
        assertEquals(user.hashCode(), another.hashCode());
    }

    @Test
    public void shouldReturnTrueForSameObjectEqualsTest() {
        assertTrue(user.equals(user));
    }

    @Test
    public void shouldReturnFalseForAnotherTypeEqualsTest() {
        assertFalse(user.equals(1));
    }
}