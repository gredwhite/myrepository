package com.peterservice.palindrome;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {
    @InjectMocks
    @Spy
    Game game = new Game("firstUser");
    @Mock
    UsersHolder usersHolder;
    @Mock
    User currentUser;
    @Mock
    User user;

    @Test
    public void testChangeUserNew() throws Exception {
        when(usersHolder.getUserByName("userName")).thenReturn(Optional.ofNullable(null));

        game.changeUser("userName");

        verify(game).createNewUser("userName");
    }

    @Test
    public void testChangeUserOld() throws Exception {
        when(usersHolder.getUserByName("userName")).thenReturn(Optional.ofNullable(user));

        game.changeUser("userName");

        verify(game, never()).createNewUser("userName");
    }

    @Test
    public void shouldReturnFalseIfWordIsDuplicatedSuggestWordTest() throws Exception {
        when(currentUser.addWord(anyString())).thenReturn(false);

        assertFalse(game.suggestWord("12321"));
    }

    @Test
    public void shouldReturnTrueIfWordNewSuggestWordTest() throws Exception {
        when(currentUser.addWord(anyString())).thenReturn(true);

        Assert.assertTrue(game.suggestWord("12321"));
    }

    @Test
    public void shouldReturnFalseIsWordIsNotPolyndromSuggestWordTest() throws Exception {
        assertFalse(game.suggestWord("qwerty"));
    }

    @Test
    public void testGetCurrentUserScore() throws Exception {
        when(currentUser.getScore()).thenReturn(20);

        assertEquals(20, game.getCurrentUserScore());
    }

    @Test
    public void testGetCurrentUserAcceptedWords() throws Exception {
        Set<String> stringSet = new HashSet<>();
        stringSet.add("12321");
        stringSet.add("qwewq");
        when(currentUser.getWords()).thenReturn(stringSet);

        assertThat(stringSet, is(game.getCurrentUserAcceptedWords()));
    }

    @Test
    public void testGetScores() throws Exception {
        Map<String, Integer> map = new HashMap<>();
        map.put("user1", 12);
        map.put("user2", 32);
        when(usersHolder.getScores(Game.TOP_USER_COUNT)).thenReturn(map);

        assertTrue(map.equals(game.getScores()));


    }
}