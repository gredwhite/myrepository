package com.peterservice.palindrome;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by redwhite on 03.10.2015.
 */
public class User {
    private String name;
    private Set<String> words = new HashSet<>();
    private int score = 0;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Method for getting current score
     *
     * @return user score
     */
    public Integer getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return Objects.equals(((User) o).name, name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    /**
     * Method for adding word to user
     *
     * @param word
     * @return true if word added and false otherwise
     */
    public boolean addWord(String word) {
        if (words.add(word)) {
            score += word.length();
            return true;
        }
        return false;
    }

    public Set<String> getWords() {
        return Collections.unmodifiableSet(words);
    }
}
