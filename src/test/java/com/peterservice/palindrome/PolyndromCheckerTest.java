package com.peterservice.palindrome;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PolyndromCheckerTest {
    @Test
    public void shouldReturnTrueIfInputStringIsPolyndromCheckTest(){
        assertTrue(PolyndromChecker.check("123454321"));
    }
    @Test
    public void shouldReturnFlseIfInputStringIsNotPolyndromCheckTest(){
        assertFalse(PolyndromChecker.check("qwerty"));
    }
}
